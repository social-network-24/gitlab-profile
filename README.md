This project was developed by following an online tutorial ([React / GraphQL Course - Build a social media app (MERNG Stack)
](https://youtu.be/n1mdAPFq2Os)) to gain practical experience and enhance skills.

[Demo](https://hilarious-begonia-26aa2d.netlify.app/).